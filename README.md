# Introduction to Kubernetes

## Basic kubectl

Run the following:

    kubectl version

If you want to update your client: https://kubernetes.io/docs/tasks/tools/

Run `kubectl get nodes` to see the nodes in your cluster.

Run `kubectl --help` to see available commands

Let's see what apps are running on our cluster

    kubectl get all

Let's run a Hello World!

    kubectl run hello-world --image=nginx --command -- echo "Hello World!"

Now something more interesting. This will create a simple deployment with 2 replica of an nginx app.

    kubectl create deployment my-nginx --image=nginx --replicas=2

Describe the deployment

    kubectl describe deployment my-nginx

You can view the logs of a pod

    kubectl logs $(kubectl get pods --selector=app=my-nginx -o jsonpath='{.items[0].metadata.name}')

You can exec into a pod

    kubectl exec -it $(kubectl get pods --selector=app=my-nginx -o jsonpath='{.items[0].metadata.name}') -- bash

Now let's make our deployment accesible within the cluster

    kubectl expose deployment my-nginx --port=80

Now let's remove this service and create another one where we can also access the app from the host machine

    kubectl delete service my-nginx
    kubectl expose --type=NodePort deployment my-nginx --port=80

Other Service types shown here: https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types

Remove all resources we created:

    kubectl delete service my-nginx
    kubectl delete deployment my-nginx

Kubectl cheat sheet: https://kubernetes.io/docs/reference/kubectl/cheatsheet/ 

---

## Manifests / Templates

We want our infrastructure to be code!  

A Manifest or template is expressing your resources in the form of a yaml file.

Starting from scratch is also a pain. Kubectl can help us get started.

    kubectl create deployment my-nginx --image=nginx --replicas=2 -o yaml --dry-run=client >> my-nginx.yaml

You can run the following to apply our template.

    kubectl apply -f my-nginx.yaml

We can now of course add our service to our template.

    kubectl expose --type=NodePort deployment my-nginx --port=80 -o yaml --dry-run=client >> my-nginx.yaml

Make sure to put a `---` separator between your resources in your template.

Take a look at resource documentation for additional options: https://kubernetes.io/docs/reference/kubernetes-api/

* How about adding some environment variables?

* Find out how you can add resource configurations to your containers.

* Add scaling to your deployment using a HorizontalPodAutoscaler.

> Warning: Docker Desktop K8s doesn't come with a metrics server which is required for autoscaling to work. If you want to try it out, follow instruction here: https://www.sujaypillai.dev/2021/02/2021-02-02-enable-metrics-server-on-docker-desktop/

---

## ConfigMaps and Secrets

We can generate the yaml

    kubectl create configmap nginx-config --from-literal=key1=config1 --from-literal=key2=config2 -o yaml --dry-run=client

How do we apply this to our deployment?

Let's create a secret

    kubectl create secret generic nginx-secret --from-literal=key1=supersecret --from-literal=key2=topsecret -o yaml --dry-run=client

Apply these secrets to the deployment.

Configmaps and Secrets are resources and can be observed through kubectl (if you have permission).

---

## But wait there's more... much much more.

* Resources
    * Ingress
    * Job / Cronjob
    * Volume
    * Authentication
    * Authorization
    * Namespaces
    * Resource and Network Policies
* Templating / Software distribution
    * Kustomize
    * Helm
* Lots of available tools to add to your cluster
